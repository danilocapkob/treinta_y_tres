import pytest
from truco import *


def test_carta_init():
    cart = Carta()
    assert cart.rango == 1 and cart.palo == 0


def test_carta_igual_palo():
    cart1 = Carta(0, 1)
    cart2 = Carta(1, 1)
    assert cart1.igual_palo(cart2)


def test_carta_igual_rango():
    cart1 = Carta(0, 1)
    cart2 = Carta(0, 4)
    assert cart1.igual_rango(cart2)


def test_mazo_init():
    mazo = Mazo()
    tamanio_mazo = len(mazo.mazo)
    assert tamanio_mazo == 40


def test_mazo_quitar_carta():
    mazo = Mazo()
    tamanio_mazo = len(mazo.mazo)
    mazo.quitar_carta()
    assert len(mazo.mazo) == tamanio_mazo - 1


def test_mazo_agregar_carta():
    mazo = Mazo()
    tamanio_mazo = len(mazo.mazo)
    cart = Carta()
    mazo.agregar_carta(cart)
    assert len(mazo.mazo) == tamanio_mazo + 1


def test_mano_init():
    mano = Mano()
    assert len(mano.mazo) == 0


def test_mano_puntos1():
    mano = Mano()
    for i in range(6, 9):  # construye 6, 7 y sota de espadas
        cart = Carta(i, 0)
        mano.agregar_carta(cart)
    puntos = mano.puntos_mano()
    assert puntos == 33


def test_mano_puntos2():
    mano = Mano()
    for i in range(3):
        cart = Carta(1, i)
        mano.agregar_carta(cart)
    puntos = mano.puntos_mano()
    assert puntos == 0


def test_mano_puntos3():
    mano = Mano()
    cart = Carta(1, 0)  # 1 de espadas
    mano.agregar_carta(cart)
    for i in range(5, 7):
        cart = Carta(i, 0)
        mano.agregar_carta(cart)
    puntos = mano.puntos_mano()
    assert puntos == 31


def test_mano_puntos4():
    mano = Mano()
    cart = Carta(1, 0)  # 1 de espadas
    mano.agregar_carta(cart)
    for i in range(8, 10):
        cart = Carta(i, 0)
        mano.agregar_carta(cart)
    puntos = mano.puntos_mano()
    assert puntos == 21
