import random


class Carta:
    """Representación de una carta.
    """

    palos = {0: "Espadas", 1: "Bastos", 2: "Oros", 3: "Copas"}
    rangos = {1: "uno", 2: "dos", 3: "tres", 4: "cuatro", 5: "cinco",
              6: "seis", 7: "siete", 8: "sota", 9: "caballo", 10: "rey"}

    def __init__(self, rango=1, palo=0):
        self.rango = rango
        self.palo = palo

    def __str__(self):
        carta = f"[{self.rangos[self.rango]} de"
        carta += f" {self.palos[self.palo]}]"
        return carta

    def igual_palo(self, carta):
        return self.palo == carta.palo

    def igual_rango(self, carta):
        return self.rango == carta.rango

    """
    def envido_tanto(self, carta2):
        puntos = 0
        if (self.rango >= 8) and (carta2.rango >= 8):
            puntos = 20
        elif (self.rango >= 8) and (carta2.rango < 8):
            puntos = 20 + carta2.rango
        elif (self.rango < 8) and (carta2.rango >= 8):
            puntos = 20 + self.rango
        else:
            puntos = self.rango + carta2.rango + 20
        return puntos
    """


class Mazo:
    """Representa el mazo de cartas, en total 40, 10 de cada palo.
    Lista de 40 'Carta'
    """

    def __init__(self):
        """Inicializa el mazo como una lista de 40 'Carta'
        """
        self.mazo = []
        for palo in range(4):
            for rango in range(1, 11):
                carta = Carta(rango, palo)
                self.mazo.append(carta)

    def mostrar_cartas(self):
        """Muestra el mazo de cartas
        """
        print()
        for num, carta in enumerate(self.mazo):
            if num % 10 == 0:
                print()
            print(carta)
        print()

    def quitar_carta(self):
        """Saca del Mazo una carta del final de la lista

        Returns:
            class Mazo: el mazo disminuido en una carta
        """
        return self.mazo.pop()

    def agregar_carta(self, carta):
        """Agrega una carta al final de la lista

        Args:
            carta (class Carta): agrega una carta al final de la lista Mazo
        """
        self.mazo.append(carta)

    def barajar(self):
        random.shuffle(self.mazo)


class Mano(Mazo):
    """Representa una mano para un jugador.

    Args:
        Mazo (class): mazo de 40 cartas
    """

    def __init__(self, jugador=''):
        """Inicia una mano, con la etiqueta 'jugador', representada por una lista

        Args:
            jugador (str, optional): describe de que jugador es la mano. Defaults to ''.
        """
        self.jugador = jugador
        self.mazo = []

    def mano_con_33(self):
        """Retorna si una mano tiene el puntaje 33 del envido para el truco.

        Returns:
            bool: si la mano posee 33 puntos del envido retorna True, caso contrario retorna False
        """
        return self.puntos_mano() == 33

    def puntos_mano(self):
        """Retorna los puntos del envido, solo si tiene dos cartas del mismo palo, no considero si tiene todos de distinto palo.

        Returns:
            int: los puntos del envido con dos cartas del mismo palo.
        """
        puntos = 0
        for carta1 in self.mazo[:2]:
            for carta2 in self.mazo[1:]:
                if (carta1.igual_palo(carta2) and
                        not carta1.igual_rango(carta2)):
                    puntos_new = envido_tanto(carta1, carta2)
                    if puntos < puntos_new:
                        puntos = puntos_new
        return puntos


class Partida:
    """Representa una partida de truco.
    """
    jugadores = ["Jugador 1", "Jugador 2", "Jugador 3",
                 "Jugador 4", "Jugador 5", "Jugador 6"]

    def __init__(self, num=6):
        """Inicia una partida de truco

        Args:
            num (int, optional): número de jugadores. Defaults to 6.
        """
        self.num = num
        self.partida = {}

        for jugador in self.jugadores[:self.num]:
            self.partida[jugador] = Mano(jugador)

    def mostrar_partida(self):
        """Muestra las cartas que posee cada jugador
        """
        for jugador in self.partida:
            print(jugador, end='')
            self.partida[jugador].mostrar_cartas()

    def repartir(self, mazo):
        """Genera la mano para cada jugador repartiendo 3 cartas a cada uno.

        Args:
            mazo (class Mazo): el mazo d 40 cartas
        """
        for jugador in self.jugadores[:self.num] * 3:
            carta = mazo.quitar_carta()
            self.partida[jugador].agregar_carta(carta)


def envido_tanto(carta1, carta2):
    """Retorna los puntos, según la regla del envido, dadas dos cartas.

    Args:
        carta1 (class Carta): carta de un palo y un rango determinado
        carta2 (class Carta): carta de un palo y un rango determinado

    Returns:
        int: los puntos según dos cartas dadas
    """
    puntos = 0
    if (carta1.rango >= 8) and (carta2.rango >= 8):
        puntos = 20
    elif (carta1.rango >= 8) and (carta2.rango < 8):
        puntos = 20 + carta2.rango
    elif (carta1.rango < 8) and (carta2.rango >= 8):
        puntos = 20 + carta1.rango
    else:
        puntos = carta1.rango + carta2.rango + 20
    return puntos
